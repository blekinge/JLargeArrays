/*
 * JLargeArrays
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jlargearrays;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.visnow.jlargearrays.FloatingPointAsserts.*;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class ComplexDoubleLargeArrayTest extends LargeArrayTest
{

    public ComplexDoubleLargeArrayTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testEmptyComplexDoubleLargeArray()
    {
        ComplexDoubleLargeArray a = new ComplexDoubleLargeArray(0);
        assertEquals(0, a.length());
        Throwable e = null;
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e = null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        
        a = new ComplexDoubleLargeArray(0, new double[]{1,2}, true);
        assertEquals(0, a.length());
        assertTrue(a.isConstant());
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e= null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
    }

    @Test
    public void testComplexDoubleLargeArrayEqualsHashCode()
    {
        ComplexDoubleLargeArray a = new ComplexDoubleLargeArray(10);
        ComplexDoubleLargeArray b = new ComplexDoubleLargeArray(10);
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertTrue(a.hashCode() == a.hashCode(1f));
        assertTrue(a.equals(b));
        assertTrue(a.hashCode() == b.hashCode());
        assertTrue(a.hashCode() == b.hashCode(1f));
        a.set(0, new double[]{1, 2});
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testComplexDoubleLargeArrayApproximateHashCode()
    {
        ComplexDoubleLargeArray a = new ComplexDoubleLargeArray(10);
        ComplexDoubleLargeArray b = new ComplexDoubleLargeArray(10);
        a.setComplexDouble(0, new double[]{1, 2});
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));

        a = new ComplexDoubleLargeArray(10, new double[]{1, 2}, true);
        b = new ComplexDoubleLargeArray(10, new double[]{2, 3}, true);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
    }

    @Test
    public void testSerialization()
    {
        long size = 1 << 5;
        ComplexDoubleLargeArray a = new ComplexDoubleLargeArray(size);
        ComplexDoubleLargeArray b = null;
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (ComplexDoubleLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
        a = new ComplexDoubleLargeArray(size, new double[]{2, 3}, true);
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (ComplexDoubleLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
    }

    @Test
    public void testComplexDoubleLargeArrayConstant()
    {
        ComplexDoubleLargeArray a = new ComplexDoubleLargeArray(10, new double[]{2.5, 1.5}, true);
        assertTrue(a.isConstant());
        assertRelativeEquals(2.5, a.getComplexDouble(0)[0]);
        assertRelativeEquals(1.5, a.getComplexDouble(0)[1]);
        assertRelativeEquals(2.5, a.getComplexDouble(a.length - 1)[0]);
        assertRelativeEquals(1.5, a.getComplexDouble(a.length - 1)[1]);
        a.setComplexDouble(0, new double[]{3.5, 4.5});
        assertFalse(a.isConstant());
        assertRelativeEquals(3.5, a.getComplexDouble(0)[0]);
        assertRelativeEquals(4.5, a.getComplexDouble(0)[1]);
    }

    @Test
    public void testComplexDoubleLargeArrayGetSet()
    {
        ComplexDoubleLargeArray a = new ComplexDoubleLargeArray(10);
        long idx = 5;
        double[] val = {3.4, -3.7};
        a.setComplexDouble(idx, val);
        assertRelativeEquals(val[0], a.getComplexDouble(idx)[0]);
        assertRelativeEquals(val[1], a.getComplexDouble(idx)[1]);
        idx = 6;
        a.set(idx, val);
        assertRelativeEquals(val[0], a.get(idx)[0]);
        assertRelativeEquals(val[1], a.get(idx)[1]);
    }

    @Test
    public void testComplexDoubleLargeArrayGetSetNative()
    {
        ComplexDoubleLargeArray a = new ComplexDoubleLargeArray(10);
        if (a.isLarge()) {
            long idx = 5;
            double[] val = {3.4, -3.7};
            a.setToNative(idx, val);
            assertRelativeEquals(val[0], a.getFromNative(idx)[0]);
            assertRelativeEquals(val[1], a.getFromNative(idx)[1]);
        }
    }

    @Test
    public void testComplexDoubleLargeArrayGetData()
    {
        double[] data = new double[]{1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9, 10.10};
        int startPos = 1;
        int endPos = 5;
        int step = 2;
        ComplexDoubleLargeArray a = new ComplexDoubleLargeArray(data);
        double[] res = a.getComplexData(null, startPos, endPos, step);
        int idx = 0;
        for (int i = startPos; i < endPos; i += step) {
            assertRelativeEquals(data[2 * i], res[2 * idx]);
            assertRelativeEquals(data[2 * i + 1], res[2 * idx + 1]);
            idx++;
        }
    }
}
