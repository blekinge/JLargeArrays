/*
 * JLargeArrays
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jlargearrays;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class StringLargeArrayTest extends LargeArrayTest
{

    public StringLargeArrayTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testEmptyStringLargeArray()
    {
        StringLargeArray a = new StringLargeArray(0);
        assertEquals(0, a.length());
        Throwable e = null;
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e = null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        
        a = new StringLargeArray(0, 10, "test", true);
        assertEquals(0, a.length());
        assertTrue(a.isConstant());
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e= null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
    }

    @Test
    public void testStringLargeArrayEqualsHashCode()
    {
        StringLargeArray a = new StringLargeArray(10);
        StringLargeArray b = new StringLargeArray(10);
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertTrue(a.hashCode() == a.hashCode(1f));
        assertTrue(a.equals(b));
        assertTrue(a.hashCode() == b.hashCode());
        assertTrue(a.hashCode() == b.hashCode(1f));
        a.set(0, "string");
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
        a.set(0, "");
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testStringLargeArrayApproximateHashCode()
    {
        StringLargeArray a = new StringLargeArray(10);
        StringLargeArray b = new StringLargeArray(10);
        a.set(0, "string");
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
        a = new StringLargeArray(10, LargeArray.DEFAULT_MAX_STRING_LENGTH, "string1", true);
        b = new StringLargeArray(10, LargeArray.DEFAULT_MAX_STRING_LENGTH, "string2", true);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
    }

    @Test
    public void testSerialization()
    {
        long size = 1 << 5;
        StringLargeArray a = new StringLargeArray(size);
        StringLargeArray b = null;
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (StringLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
        for (long i = 0; i < size; i++) {
            a.set(i, "string" + i);
        }
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (StringLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
        a = new StringLargeArray(size, LargeArray.DEFAULT_MAX_STRING_LENGTH, "test0123ąęćńżź", true);
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (StringLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
    }

    @Test
    public void testStringLargeArrayConstant()
    {
        StringLargeArray a = new StringLargeArray(10, LargeArray.DEFAULT_MAX_STRING_LENGTH, "test0123ąęćńżź", true);
        assertTrue(a.isConstant());
        assertEquals("test0123ąęćńżź", a.get(0));
        assertEquals("test0123ąęćńżź", a.get(a.length() - 1));
        a.set(0, "aaatest0123ąęćńżź");
        assertEquals("aaatest0123ąęćńżź", a.get(0));
        assertFalse(a.isConstant());
        
        a = new StringLargeArray(10, LargeArray.DEFAULT_MAX_STRING_LENGTH, null, false);
        assertFalse(a.isConstant());
        assertEquals(null, a.get(0));

        a = new StringLargeArray(10, LargeArray.DEFAULT_MAX_STRING_LENGTH, null, true);
        assertTrue(a.isConstant());
        assertEquals(null, a.get(0));
    }

    @Test
    public void testStringLargeArrayGetSet()
    {
        StringLargeArray a = new StringLargeArray(10, 14);
        long idx = 5;
        String val1 = "test0123ąęćńżź";
        String val2 = "test";
        a.set(idx, val1);
        assertEquals(val1, a.get(idx));
        a.set(idx, val2);
        assertEquals(val2, a.get(idx));
    }

    @Test
    public void testStringLargeArrayGetSetNative()
    {
        StringLargeArray a = new StringLargeArray(10, 14);

        if (a.isLarge()) {
            long idx = 5;
            String val1 = "test0123ąęćńżź";
            String val2 = "test";
            a.setToNative(idx, val1);
            assertEquals(val1, a.getFromNative(idx));
            a.setToNative(idx, val2);
            assertEquals(val2, a.getFromNative(idx));
        }
    }
}
