/*
 * JLargeArrays
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jlargearrays;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.visnow.jlargearrays.FloatingPointAsserts.*;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class DoubleLargeArrayTest extends LargeArrayTest
{

    public DoubleLargeArrayTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testEmptyDoubleLargeArray()
    {
        DoubleLargeArray a = new DoubleLargeArray(0);
        assertEquals(0, a.length());
        Throwable e = null;
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e = null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        
        a = new DoubleLargeArray(0, 1, true);
        assertEquals(0, a.length());
        assertTrue(a.isConstant());
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e= null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
    }

    @Test
    public void testDoubleLargeArrayEqualsHashCode()
    {
        DoubleLargeArray a = new DoubleLargeArray(10);
        DoubleLargeArray b = new DoubleLargeArray(10);
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertTrue(a.hashCode() == a.hashCode(1f));
        assertTrue(a.equals(b));
        assertTrue(a.hashCode() == b.hashCode());
        assertTrue(a.hashCode() == b.hashCode(1f));
        a.setDouble(0, 1);
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testDoubleLargeArrayApproximateHashCode()
    {
        DoubleLargeArray a = new DoubleLargeArray(10);
        DoubleLargeArray b = new DoubleLargeArray(10);
        a.setDouble(0, 1d);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
        a = new DoubleLargeArray(10, 0d, true);
        b = new DoubleLargeArray(10, 1d, true);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
    }

    @Test
    public void testSerialization()
    {
        long size = 1 << 5;
        DoubleLargeArray a = new DoubleLargeArray(size);
        DoubleLargeArray b = null;
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (DoubleLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
        a = new DoubleLargeArray(size, 2.0, true);
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (DoubleLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
    }

    @Test
    public void testDoubleLargeArrayConstant()
    {
        DoubleLargeArray a = new DoubleLargeArray(10, 2.5, true);
        assertTrue(a.isConstant());
        assertRelativeEquals(2.5, a.getDouble(0));
        assertRelativeEquals(2.5, a.getDouble(a.length() - 1));
        a.setDouble(0, 3.5);
        assertRelativeEquals(3.5, a.getDouble(0));
        assertFalse(a.isConstant());
    }

    @Test
    public void testDoubleLargeArrayGetSet()
    {
        DoubleLargeArray a = new DoubleLargeArray(10);
        long idx = 5;
        double val = 3.4;
        a.setDouble(idx, val);
        assertRelativeEquals(val, a.getDouble(idx));
        idx = 6;
        a.set(idx, val);
        assertRelativeEquals(val, a.get(idx));
    }

    @Test
    public void testDoubleLargeArrayGetSetNative()
    {
        DoubleLargeArray a = new DoubleLargeArray(10);
        if (a.isLarge()) {
            long idx = 5;
            double val = 3.4;
            a.setToNative(idx, val);
            assertRelativeEquals(val, a.getFromNative(idx));
        }
    }

    @Test
    public void testDoubleLargeArrayGetData()
    {
        double[] data = new double[]{1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9, 10.10};
        long startPos = 2;
        long endPos = 7;
        long step = 2;
        DoubleLargeArray a = new DoubleLargeArray(data);
        double[] res = a.getDoubleData(null, startPos, endPos, step);
        int idx = 0;
        for (long i = startPos; i < endPos; i += step) {
            assertRelativeEquals(data[(int) i], res[idx++]);
        }
    }
}
