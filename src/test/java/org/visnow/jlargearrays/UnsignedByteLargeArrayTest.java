/*
 * JLargeArrays
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jlargearrays;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.visnow.jlargearrays.FloatingPointAsserts.*;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class UnsignedByteLargeArrayTest extends LargeArrayTest
{

    public UnsignedByteLargeArrayTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testEmptyUnsignedByteLargeArray()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(0);
        assertEquals(0, a.length());
        Throwable e = null;
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e = null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);

        a = new UnsignedByteLargeArray(0, (short) 1, true);
        assertEquals(0, a.length());
        assertTrue(a.isConstant());
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e = null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
    }

    @Test
    public void testUnsignedByteLargeArrayEqualsHashCode()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(10);
        UnsignedByteLargeArray b = new UnsignedByteLargeArray(10);
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertTrue(a.hashCode() == a.hashCode(1f));
        assertTrue(a.equals(b));
        assertTrue(a.hashCode() == b.hashCode());
        assertTrue(a.hashCode() == b.hashCode(1f));
        a.setUnsignedByte(0, (short) 1);
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testUnsignedByteLargeArrayApproximateHashCode()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(10);
        UnsignedByteLargeArray b = new UnsignedByteLargeArray(10);
        a.setUnsignedByte(0, (short) 1);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
        a = new UnsignedByteLargeArray(10, (short) 0, true);
        b = new UnsignedByteLargeArray(10, (short) 1, true);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
    }

    @Test
    public void testSerialization()
    {
        long size = 1 << 5;
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(size);
        UnsignedByteLargeArray b = null;
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (UnsignedByteLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
        a = new UnsignedByteLargeArray(size, (short) 2, true);
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (UnsignedByteLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
    }

    @Test
    public void testUnsignedByteLargeArrayConstant()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(10, (short) 225, true);
        assertTrue(a.isConstant());
        assertEquals(225, a.getUnsignedByte(0));
        assertEquals(225, a.getUnsignedByte(a.length() - 1));
        a.setUnsignedByte(0, (short) 200);
        assertEquals(200, a.getUnsignedByte(0));
        assertFalse(a.isConstant());
    }

    @Test
    public void testUnsignedByteLargeArrayGetSet()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(10);
        long idx = 5;
        short val = 255;
        a.set(idx, val);
        assertEquals(val, (a.get(idx)).shortValue());
    }

    @Test
    public void testUnsignedByteLargeArrayGetSetNative()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(10);
        if (a.isLarge()) {
            long idx = 5;
            byte val = -100;
            a.setToNative(idx, val);
            assertEquals(val, (byte) a.getFromNative(idx));
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetSetBoolean()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(10);
        long idx = 5;
        boolean val = true;
        a.setBoolean(idx, val);
        assertEquals(val, a.getBoolean(idx));
    }

    @Test
    public void testUnsignedByteLargeArrayGetSetByte()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(10);
        long idx = 5;
        byte val = 127;
        a.setByte(idx, val);
        assertEquals(val, a.getByte(idx));
    }

    @Test
    public void testUnsignedByteLargeArrayGetSetUnsignedByte()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(10);
        long idx = 5;
        short val = 255;
        a.setUnsignedByte(idx, val);
        assertEquals((0xFF & val), a.getUnsignedByte(idx));
    }

    @Test
    public void testUnsignedByteLargeArrayGetSetShort()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(10);
        long idx = 5;
        short val = 500;
        a.setShort(idx, val);
        assertEquals((0xFF & val), a.getShort(idx));
    }

    @Test
    public void testUnsignedByteLargeArrayGetSetInt()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(10);
        long idx = 5;
        int val = 500;
        a.setInt(idx, val);
        assertEquals((0xFF & val), a.getInt(idx));
    }

    @Test
    public void testUnsignedByteLargeArrayGetSetLong()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(10);
        long idx = 5;
        long val = 500;
        a.setLong(idx, val);
        assertEquals((0xFF & val), a.getLong(idx));
    }

    @Test
    public void testUnsignedByteLargeArrayGetSetFloat()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(10);
        long idx = 5;
        float val = 500;
        a.setFloat(idx, val);
        assertRelativeEquals((0xFF & (int) val), a.getFloat(idx));
    }

    @Test
    public void testUnsignedByteLargeArrayGetSetDouble()
    {
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(10);
        long idx = 5;
        double val = 500;
        a.setDouble(idx, val);
        assertRelativeEquals((0xFF & (long) val), a.getDouble(idx));
    }

    @Test
    public void testUnsignedByteLargeArrayGetData()
    {
        byte[] data = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        byte[] res = a.getData();
        for (int i = 0; i < data.length; i++) {
            assertEquals(data[i], res[i]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetByteData1()
    {
        byte[] data = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        byte[] res = a.getByteData();
        for (int i = 0; i < data.length; i++) {
            assertEquals(data[i], res[i]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetByteData2()
    {
        byte[] data = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        long startPos = 2;
        long endPos = 7;
        long step = 2;
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        byte[] res = a.getByteData(null, startPos, endPos, step);
        int idx = 0;
        for (long i = startPos; i < endPos; i += step) {
            assertEquals(data[(int) i], res[idx++]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetUnsignedByteData1()
    {
        short[] data = new short[]{1, 2, 3, 4, 5, 6, 7, 80, 190, 255};
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        short[] res = a.getUnsignedByteData();
        for (int i = 0; i < data.length; i++) {
            assertEquals(data[i], res[i]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetUnsignedByteData2()
    {
        short[] data = new short[]{1, 2, 3, 4, 5, 6, 7, 80, 190, 255};
        long startPos = 2;
        long endPos = 7;
        long step = 2;
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        short[] res = a.getUnsignedByteData(null, startPos, endPos, step);
        int idx = 0;
        for (long i = startPos; i < endPos; i += step) {
            assertEquals(data[(int) i], res[idx++]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetShortData1()
    {
        short[] data = new short[]{1, 2, 3, 4, 5, 6, 7, 80, 190, 255};
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        short[] res = a.getShortData();
        for (int i = 0; i < data.length; i++) {
            assertEquals(data[i], res[i]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetShortData2()
    {
        short[] data = new short[]{1, 2, 3, 4, 5, 6, 7, 80, 190, 255};
        long startPos = 2;
        long endPos = 7;
        long step = 2;
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        short[] res = a.getShortData(null, startPos, endPos, step);
        int idx = 0;
        for (long i = startPos; i < endPos; i += step) {
            assertEquals(data[(int) i], res[idx++]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetIntData1()
    {
        short[] data = new short[]{1, 2, 3, 4, 5, 6, 7, 80, 190, 255};
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        int[] res = a.getIntData();
        for (int i = 0; i < data.length; i++) {
            assertEquals(data[i], res[i]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetIntData2()
    {
        short[] data = new short[]{1, 2, 3, 4, 5, 6, 7, 80, 190, 255};
        long startPos = 2;
        long endPos = 7;
        long step = 2;
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        int[] res = a.getIntData(null, startPos, endPos, step);
        int idx = 0;
        for (long i = startPos; i < endPos; i += step) {
            assertEquals(data[(int) i], res[idx++]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetLongData1()
    {
        short[] data = new short[]{1, 2, 3, 4, 5, 6, 7, 80, 190, 255};
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        long[] res = a.getLongData();
        for (int i = 0; i < data.length; i++) {
            assertEquals(data[i], res[i]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetLongData2()
    {
        short[] data = new short[]{1, 2, 3, 4, 5, 6, 7, 80, 190, 255};
        long startPos = 2;
        long endPos = 7;
        long step = 2;
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        long[] res = a.getLongData(null, startPos, endPos, step);
        int idx = 0;
        for (long i = startPos; i < endPos; i += step) {
            assertEquals(data[(int) i], res[idx++]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetFloatData1()
    {
        short[] data = new short[]{1, 2, 3, 4, 5, 6, 7, 80, 190, 255};
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        float[] res = a.getFloatData();
        for (int i = 0; i < data.length; i++) {
            assertRelativeEquals(data[i], res[i]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetFloatData2()
    {
        short[] data = new short[]{1, 2, 3, 4, 5, 6, 7, 80, 190, 255};
        long startPos = 2;
        long endPos = 7;
        long step = 2;
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        float[] res = a.getFloatData(null, startPos, endPos, step);
        int idx = 0;
        for (long i = startPos; i < endPos; i += step) {
            assertRelativeEquals(data[(int) i], res[idx++]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetDoubleData1()
    {
        short[] data = new short[]{1, 2, 3, 4, 5, 6, 7, 80, 190, 255};
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        double[] res = a.getDoubleData();
        for (int i = 0; i < data.length; i++) {
            assertRelativeEquals(data[i], res[i]);
        }
    }

    @Test
    public void testUnsignedByteLargeArrayGetDoubleData2()
    {
        short[] data = new short[]{1, 2, 3, 4, 5, 6, 7, 80, 190, 255};
        long startPos = 2;
        long endPos = 7;
        long step = 2;
        UnsignedByteLargeArray a = new UnsignedByteLargeArray(data);
        double[] res = a.getDoubleData(null, startPos, endPos, step);
        int idx = 0;
        for (long i = startPos; i < endPos; i += step) {
            assertRelativeEquals(data[(int) i], res[idx++]);
        }
    }
}
